import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Member {
  String name;
  String email;
  String phone;
  DateTime memberSince;
  DateTime birthDay;
  int subscriptionYear;

  Member(
      {required this.name,
      required this.email,
      required this.phone,
      required this.memberSince,
      required this.birthDay,
      required this.subscriptionYear});

  int age() {
    DateTime now = DateTime.now();
    if (now.month > birthDay.month ||
        (now.month == birthDay.month && now.day >= birthDay.day)) {
      return now.year - birthDay.year;
    } else {
      return now.year - birthDay.year - 1;
    }
  }

  bool activeMembership() {
    return subscriptionYear == DateTime.now().year;
  }

  static Future<Member?> getMember(User? user) async {
    assert(user != null);
    CollectionReference users =
        FirebaseFirestore.instance.collection('Members');
    DocumentSnapshot allMembers = await users.doc("AllMembers").get();
    if (!allMembers.exists) {
      print("Collection dosen't exist");
      return null;
    }

    Map<String, dynamic> data = allMembers.data() as Map<String, dynamic>;
    var userdata = await data[user!.uid.toString()].get();
    if (!userdata.exists) {
      return null;
    }
    Timestamp ind = userdata["Indmelding"];
    Timestamp bd = userdata["Fødselsdag"];
    Member result = Member(
        name: userdata["Navn"],
        email: userdata["Email"],
        phone: userdata["Telefon"],
        memberSince: ind.toDate(),
        birthDay: bd.toDate(),
        subscriptionYear: userdata["Kontingentår"]);
    return result;
  }
}
