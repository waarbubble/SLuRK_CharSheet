import 'package:slurk_char_sheet/layoutsettings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ThemedScaffold extends StatelessWidget {
  final String pageName;
  final Widget? child;
  final Drawer? drawer;
  const ThemedScaffold(
      {Key? key, required this.pageName, this.child, this.drawer})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (width > 400) width = 400;

    return Stack(children: [
      Image.asset(
        "images/background.jpg",
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
      Scaffold(
        appBar: AppBar(
          title: Row(children: [Text(this.pageName)]),
        ),
        backgroundColor: Colors.transparent,
        body: Align(
            child: Container(
                margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                decoration: BoxDecoration(
                    color: LayoutSettings.materialColor.shade900.withAlpha(200),
                    borderRadius: BorderRadius.circular(15)),
                width: width,
                child: this.child)),
        drawer: this.drawer,
      )
    ]);
  }
}
