import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:slurk_char_sheet/layoutsettings.dart';

class ThemedTextButton extends StatelessWidget {
  String text;
  void Function()? onPressed;
  ThemedTextButton({Key? key, required this.text, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        onPressed: this.onPressed,
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color?>(Colors.amber.shade900),
            side: MaterialStateProperty.all<BorderSide>(
                BorderSide(color: Colors.brown.shade900, width: 1)),
            minimumSize: MaterialStateProperty.all(Size(50, 40))),
        child: Text(
          text,
          style: LayoutSettings.textStyle,
        ));
  }
}
