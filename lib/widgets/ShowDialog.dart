import 'package:flutter/material.dart';
import 'ThemedTextField.dart';
import 'package:slurk_char_sheet/layoutsettings.dart';

void showOkDialog(BuildContext context, {required String title,required String body}) {
  // set up the button
  Widget okButton = FlatButton(
    child: Text("OK",style: LayoutSettings.textStyle,),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: ThemedText(title),
    content: ThemedText(body),
    backgroundColor: Colors.amber.shade900,
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Future<String> showInputDialog(BuildContext context,
    {required String title, required String body}) async {
  bool accepted = false;
  // set up the button
  Widget acceptButton = TextButton(
    child: Text("accept"),
    onPressed: () {
      accepted = true;
      Navigator.of(context).pop();
    },
  );

  Widget cancelButton = TextButton(
    child: Text("cancel"),
    onPressed: () {
      accepted = false;
      Navigator.of(context).pop();
    },
  );

  TextEditingController controller = TextEditingController();
  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: ThemedText(title),
    content: Container(
        height: 100,
        child: Column(children: [
          ThemedText(body),
          SizedBox(
            height: 5,
          ),
          ThemedTextField(
            controller: controller,
          )
        ])),
    backgroundColor: Colors.amber.shade900,
    actions: [cancelButton, acceptButton],
  );

  // show the dialog
  await showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      });

  String ret = "";
  if (accepted) {
    ret = controller.text;
  }
  return ret;
}
