import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ThemedText extends StatelessWidget {
  final String text;
  final Text widget;

  @override
  ThemedText(this.text, {double? fontSize, FontWeight? fontWeight, Color? bgColor})
      : widget = Text(text,
            style: TextStyle(
                backgroundColor: bgColor,
                color: Colors.amber, 
                fontSize: fontSize,
                fontWeight: fontWeight));

  @override
  Widget build(BuildContext context) {
    return this.widget;
  }
}

class ThemedTextField extends StatelessWidget {
  final TextField text;
  final Color themeColor = Color(0xFF00B2B2);
  @override
  ThemedTextField(
      {IconData? prefixIcon,
      Widget? sufixIcon,
      String? labelText,
      String? hintText,
      void Function()? onEditingComplete,
      TextEditingController? controller})
      : text = TextField(
            controller: controller,
            onEditingComplete: onEditingComplete,
            style: TextStyle(color: Colors.amber),
            decoration: InputDecoration(
                enabledBorder: new OutlineInputBorder(
                    borderSide: new BorderSide(color: Colors.amber)),
                prefixIcon: Icon(
                  prefixIcon,
                  color: Colors.amber,
                ),
                suffixIcon: sufixIcon,
                filled: true,
                fillColor: Colors.amber.shade900,
                labelStyle: TextStyle(color: Colors.amber.withAlpha(0xA0)),
                labelText: labelText,
                hintText: hintText,
                hintStyle: TextStyle(color: Colors.amber.withAlpha(100))));

  @override
  Widget build(BuildContext context) {
    return text;
  }
}
