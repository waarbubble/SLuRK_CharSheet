import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import 'package:slurk_char_sheet/authentication_service.dart';
import 'package:slurk_char_sheet/pages/signin_page.dart';
import 'package:slurk_char_sheet/pages/home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyApp()));
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp();

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _fbApp,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print("you have an error! ${snapshot.error.toString()}");
          return Text("Somthing went Wrong");
        } else if (snapshot.hasData) {
          return MultiProvider(providers: [
            Provider<AuthenticationService>(
                create: (_) => AuthenticationService(FirebaseAuth.instance)),
            StreamProvider<User?>(
                initialData: null,
                create: (context) =>
                    context.read<AuthenticationService>().authStateChanges)
          ], child: AuthenticationWrapper());
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}

class AuthenticationWrapper extends StatelessWidget {
  const AuthenticationWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firbaseuser = context.watch<User?>();

    if (firbaseuser != null) {
      return HomePage();
    }
    return SignInPage();
  }
}
