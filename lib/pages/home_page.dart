import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:slurk_char_sheet/widgets/ShowDialog.dart';
import 'package:slurk_char_sheet/layoutsettings.dart';
import 'package:slurk_char_sheet/widgets/themedbutton.dart';
import 'package:slurk_char_sheet/widgets/themedscaffold.dart';
import 'package:slurk_char_sheet/pages/member_page.dart';
import 'package:slurk_char_sheet/authentication_service.dart';

class HomePage extends StatelessWidget {
  final MaterialColor color;

  HomePage({Key? key, this.color = Colors.amber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (width > 400) width = 400;

    return ThemedScaffold(
      pageName: "home",
      child: Column(children: [
        Text(
          "SLURK Profil",
          style: LayoutSettings.titleStyle,
        ),
        ThemedTextButton(
            text: "Medlemsbevis",
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          MembershipProf(user: FirebaseAuth.instance.currentUser)));
            }),
      ]),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Text("Sign Out"),
              onTap: () {
                context.read<AuthenticationService>().signOut();
              },
            )
          ],
        ),
      ),
    );
  }
}
