import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:slurk_char_sheet/authentication_service.dart';
import 'package:provider/provider.dart';
import 'package:slurk_char_sheet/widgets/ShowDialog.dart';

class SignInPage extends StatelessWidget {
  final TextEditingController email_ctrl = TextEditingController();
  final TextEditingController password_ctrl = TextEditingController();
  final MaterialColor color;

  SignInPage({Key? key, this.color = Colors.amber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    if (width > 400) width = 400;

    return Stack(children: [
      Image.asset(
        "images/background.jpg",
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
      Scaffold(
        appBar: AppBar(title: Text("Sign In")),
        backgroundColor: Colors.transparent,
        body: Align(
            child: Container(
                margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
                decoration: BoxDecoration(
                    color: color.shade900.withAlpha(200),
                    borderRadius: BorderRadius.circular(15)),
                width: width,
                height: 400,
                child: Column(children: [
                  Center(
                      child: Text("Sign in",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: color,
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                          ))),
                  SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: email_ctrl,
                    decoration: InputDecoration(
                      enabledBorder: new OutlineInputBorder(
                          borderSide: new BorderSide(color: Colors.black)),
                      prefixIcon: Icon(
                        Icons.email,
                        color: color.shade900,
                      ),
                      filled: true,
                      fillColor: color,
                      labelStyle: TextStyle(color: color.shade900),
                      labelText: "Email",
                      hintText: "Enter email of account",
                      hintStyle: TextStyle(color: color.shade900),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextField(
                      controller: password_ctrl,
                      decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.black)),
                        prefixIcon: Icon(
                          Icons.vpn_key,
                          color: color.shade900,
                        ),
                        filled: true,
                        fillColor: color,
                        labelStyle: TextStyle(color: color.shade900),
                        labelText: "Password",
                        hintText: "Enter password",
                        hintStyle: TextStyle(color: color.shade900),
                      ),
                      obscureText: true),
                  SizedBox(
                    height: 15,
                  ),
                  OutlinedButton(
                      onPressed: () {
                        context
                            .read<AuthenticationService>()
                            .signIn(
                                email: email_ctrl.text,
                                password: password_ctrl.text)
                            .then((value) {
                          if (value != "Signed in") {
                            showOkDialog(context,
                                title: "SignIn Failed",
                                body:
                                    "Either the password or the email was not recognized");
                          }
                        });
                      },
                      style: ButtonStyle(
                          side: MaterialStateProperty.all<BorderSide>(
                              BorderSide(
                                  color: Colors.brown.shade900, width: 2)),
                          minimumSize: MaterialStateProperty.all(Size(50, 40))),
                      child: Text(
                        "Sign in",
                        style: TextStyle(fontSize: 22),
                      ))
                ]))),
      )
    ]);
  }
}
