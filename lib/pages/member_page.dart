import 'package:slurk_char_sheet/layoutsettings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:slurk_char_sheet/widgets/themedscaffold.dart';
import 'package:slurk_char_sheet/database/member.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MembershipProf extends StatefulWidget {
  User? user;
  MembershipProf({Key? key, required this.user}) : super(key: key);

  @override
  _MembershipProfState createState() => _MembershipProfState();
}

class _MembershipProfState extends State<MembershipProf> {
  Member? member;
  Future<Member?>? fmem;

  @override
  Widget build(BuildContext context) {
    if (fmem == null) {
      fmem = Member.getMember(widget.user);
      fmem?.then((mem) {
        member = mem;
      });
      print(widget.user?.uid);
    }
    return ThemedScaffold(
        pageName: "Medlems Bevis",
        child: FutureBuilder(
            future: fmem,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
              } else if (snapshot.hasData) {
                Text membership = Text(
                  "Medlemskab: InAktivit",
                  style: LayoutSettings.textStyle,
                );
                if (member!.activeMembership()) {
                  membership = Text(
                    "Medlemskab: Aktivt",
                    style: LayoutSettings.textStyle,
                  );
                }
                return Container(
                    padding: LayoutSettings.padding,
                    height: 200,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "MedlemsKort",
                          style: LayoutSettings.titleStyle,
                        ),
                        Text(
                          "Navn: ${member!.name}",
                          style: LayoutSettings.textStyle,
                        ),
                        Text(
                          "Alder: ${member!.age()} År",
                          style: LayoutSettings.textStyle,
                        ),
                        membership
                      ],
                    ));
              } else if (snapshot.connectionState == ConnectionState.done) {
                return Center(child: Text("Failed to find member certificate"));
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }));
  }
}
