import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LayoutSettings {
  static final TextStyle titleStyle = TextStyle(
    color: Colors.amber,
    fontWeight: FontWeight.bold,
    fontSize: 32,
  );
  static final TextStyle textStyle = TextStyle(
    color: Colors.amber,
    fontSize: 18,
  );

  static final MaterialColor materialColor = Colors.amber;
  static final EdgeInsets padding = EdgeInsets.all(20);
}
